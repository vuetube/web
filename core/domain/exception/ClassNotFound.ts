import { ErrorResponse } from "./ErrorResponse";

export class ClassNotFound extends ErrorResponse {
  constructor(message: string) {
    super(message, 500, "class_not_found");
  }
}
