import { ErrorResponse } from "./ErrorResponse";

export class ErrorHandler extends ErrorResponse {
  constructor(message: string, statusCode?: number, reason?: string) {
    super(message, statusCode, reason);
  }

  static fromObject(object: any) {
    return new ErrorHandler(object.message, object.code, object.reason);
  }
}
