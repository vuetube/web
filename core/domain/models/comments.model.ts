import { UserInfo } from "./user_info.model";
import { Reply } from "@core/domain/models/reply.model";
import { VideoType } from "@core/domain/models/video.model";

export class CommentsType {
  text!: string;
  userId!: UserInfo;
  videoId!: VideoType;
  createdAt!: Date;
  updatedAt!: Date;
  replies!: Reply[];
  id!: string;

  constructor(
    text: string,
    userId: UserInfo,
    videoId: VideoType,
    createdAt: Date,
    updatedAt: Date,
    replies: Reply[],
    id: string
  ) {
    this.text = text;
    this.userId = userId;
    this.videoId = videoId;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.replies = replies;
    this.id = id;
  }
}
