import { Type } from "@/shared/enums";

export class Feeling {
  type!: Type;
  userId!: number;
  videoId!: number;
  createdAt!: Date;
  updatedAt!: Date;

  constructor(
    type: Type,
    userId: number,
    videoId: number,
    createdAt: Date,
    updatedAt: Date
  ) {
    this.type = type;
    this.userId = userId;
    this.videoId = videoId;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }
}
