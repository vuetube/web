import { VideoType } from "./video.model";
import { UserInfo } from "./user_info.model";

export class History {
  id!: string;
  type!: string;
  searchText?: string;
  videoId?: VideoType;
  userId!: UserInfo;
  createdAt!: Date;
  updatedAt!: Date;

  constructor(
    id: string,
    type: string,
    searchText: string,
    videoId: VideoType,
    userId: UserInfo,
    createdAt: Date,
    updatedAt: Date
  ) {
    this.id = id;
    this.type = type;
    this.searchText = searchText;
    this.videoId = videoId;
    this.userId = userId;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }
}
