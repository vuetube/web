import { VideoModel } from "@/shared/models/video.model";
import { UserInfo } from "@core/domain/models/user_info.model";
import { VideoType } from "@core/domain/models/video.model";

export class SearchVideoType {
  createdAt!: Date;
  description!: string;
  id!: string;
  status!: string;
  thumbnailUrl!: string;
  title!: string;
  updatedAt!: Date;
  url!: string;
  userId!: UserInfo;
  views!: number;

  constructor(
    createdAt: Date,
    description: string,
    id: string,
    status: string,
    thumbnailUrl: string,
    title: string,
    updatedAt: Date,
    url: string,
    userId: UserInfo,
    views: number
  ) {
    this.createdAt = createdAt;
    this.description = description;
    this.id = id;
    this.status = status;
    this.thumbnailUrl = thumbnailUrl;
    this.title = title;
    this.updatedAt = updatedAt;
    this.url = url;
    this.userId = userId;
    this.views = views;
  }
}

export class SearchUserType {
  photoUrl!: string;
  channelName!: string;
  email!: string;
  createdAt!: Date;
  updatedAt!: Date;
  subscribers!: number;
  videos?: VideoType[];
  id!: string;

  constructor(
    photoUrl: string,
    channelName: string,
    email: string,
    createdAt: Date,
    updatedAt: Date,
    subscribers: number,
    videos: VideoType[],
    id: string
  ) {
    (this.photoUrl = photoUrl),
      (this.channelName = channelName),
      (this.email = email),
      (this.createdAt = createdAt),
      (this.updatedAt = updatedAt),
      (this.subscribers = subscribers),
      (this.videos = videos),
      (this.id = id);
  }
}
