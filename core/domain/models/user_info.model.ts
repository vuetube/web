import { VideoModel } from "@/shared/models/video.model";

export class UserInfo {
  photoUrl!: string;
  role!: string;
  channelName!: string;
  email!: string;
  createdAt!: Date;
  updatedAt!: Date;
  __v?: number;
  subscribers!: number;
  video?: VideoModel[];
  id!: string;

  constructor(
    photoUrl: string,
    role: string,
    channelName: string,
    email: string,
    createdAt: Date,
    updatedAt: Date,
    subscribers: number,
    id: string
  ) {
    this.photoUrl = photoUrl;
    this.role = role;
    this.channelName = channelName;
    this.email = email;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.subscribers = subscribers;
    this.id = id;
  }

  static fromObject(ob: UserInfo): UserInfo {
    return new UserInfo(
      ob.photoUrl,
      ob.role,
      ob.channelName,
      ob.email,
      ob.createdAt,
      ob.updatedAt,
      ob.subscribers,
      ob.id
    );
  }
}
