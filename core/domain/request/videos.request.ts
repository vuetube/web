export class VideosRequest {
  title!: string;
  description?: string;
  status?: string | "draft";
  categoryId!: string;
  url!: string;
  thumbnailUrl!: string;
}
