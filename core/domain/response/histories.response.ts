import {SuccessResponse} from "@core/domain/response/success.response";
import {History} from  "@core/domain/models/history.model"
export class HistoriesResponse extends SuccessResponse {
    count?: number;
    totalPage?: number;
    pagination?: object | any;
    results!: History[]
}
export class HistoryResponse extends SuccessResponse {
    results!: History
}