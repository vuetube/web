import {SuccessResponse} from "./success.response";
import {Reply} from "../models/reply.model";
import {UserInfo} from "../models/user_info.model";
import {VideoType} from "../models/video.model";

export class SearchesResponse extends SuccessResponse {
    count?: number;
    totalPage?: number;
    pagination?: object | any;
    results !: object[]
}

export class SearchResponse extends SuccessResponse {
    results !: {
        users: UserInfo[],
        videos: VideoType[]
    }
}