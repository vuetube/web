import {SuccessResponse} from "./success.response";
import {SubscriptionType} from "../models/subscription.model";

export class SubscriptionsResponse extends SuccessResponse {
    count?: number;
    totalPage?: number;
    pagination?: object | any;
    results!:  SubscriptionType[]
}

export class SubscriptionResponse extends SuccessResponse {
    results!: SubscriptionType
}