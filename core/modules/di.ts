import { BaseModule } from "./index";
import { Container } from "typescript-ioc";

export type OtherType<T> = Function & { prototype: T };

export abstract class DI {
  static init(modules: BaseModule[]): void {
    modules.forEach(module => module.configuration());
  }

  static get<T>(key: string | OtherType<T>): T {
    if (typeof key === "string") {
      return Container.getValue(key);
    } else {
      return Container.get<T>(key);
    }
  }
}

export enum DIKeys {
  profiler = "profiler",
  apiHost = "api_host",
  staticHost = "static_host",
  guest = "guest",
  noAuthClient = "no_auth_client",
  authClient = "auth_client",
  guestService = "guest_service",
  noAuthService = "no_auth_service",
  authService = "auth_service",
  privateVideos = "private_videos",
  privateVideosService = "private_videos_Service",
  publicVideos = "public_Videos",
  publicVideosService = "public_Videos_Service"
}
