import { RegisterRequest } from "../domain/request/register.request";
import {
  LoginRequest,
  OAuthLoginRequest
} from "../domain/request/login.request";
import { BaseClient } from "../services";
import {TokenResponse, UserResponse} from "@core/domain/response/user.response";

export abstract class AuthenticationRepository {
  abstract register(request: RegisterRequest): Promise<UserResponse>;

  abstract login(request: LoginRequest): Promise<TokenResponse>;

  abstract getUserLogged(): Promise<UserResponse>;

  abstract oauthLogin(request: OAuthLoginRequest): Promise<TokenResponse>;

  abstract updateDetails(
    email: string,
    channelName: string
  ): Promise<UserResponse>;

  abstract updateAvatar(url: string): Promise<UserResponse>;

  abstract updatePassword(
    currentPassword: string,
    newPassword: string
  ): Promise<UserResponse>;

  abstract forgotPassword(email: string): Promise<UserResponse>;

  abstract resetPassword(
    token: string,
    newPassword: string
  ): Promise<UserResponse>;

  abstract getUser(id: string): Promise<UserResponse>;

  abstract logout(): Promise<UserResponse>;
}

export class HttpAuthenticationRepository implements AuthenticationRepository {
  private apiPath = "/api/v1/auth";

  constructor(private httpClient: BaseClient) {}

  login(request: LoginRequest): Promise<TokenResponse> {
    return this.httpClient.post(
      `${this.apiPath}/login`,
      request
    );
  }

  register(request: RegisterRequest): Promise<UserResponse> {
    return this.httpClient.post(
      `${this.apiPath}/register`,
      request
    );
  }

  getUserLogged(): Promise<UserResponse> {
    return this.httpClient.get(`${this.apiPath}/me`);
  }

  oauthLogin(request: OAuthLoginRequest): Promise<TokenResponse> {
    return this.httpClient.post(
      `${this.apiPath}/google`,
      request
    );
  }

  updateDetails(email: string, channelName: string): Promise<UserResponse> {
    return this.httpClient.put(
      `${this.apiPath}/update_details`,
      { email, channelName }
    );
  }

  updateAvatar(url: string): Promise<UserResponse> {
    return this.httpClient.put(`${this.apiPath}/avatar`, {
      avatar: url
    });
  }

  updatePassword(
    currentPassword: string,
    newPassword: string
  ): Promise<UserResponse> {
    return this.httpClient.put(
      `${this.apiPath}/update_password`,
      { currentPassword, newPassword }
    );
  }

  forgotPassword(email: string): Promise<UserResponse> {
    return this.httpClient.post(
      `${this.apiPath}/forgot_password`,
      { email }
    );
  }

  resetPassword(token: string, newPassword: string): Promise<UserResponse> {
    return this.httpClient.put(
      `${this.apiPath}/reset_password/${token}`,
      { newPassword }
    );
  }

  getUser(id: string): Promise<UserResponse> {
    return this.httpClient.get(`${this.apiPath}/users/${id}`);
  }

  logout(): Promise<UserResponse> {
    return this.httpClient.post(`${this.apiPath}/logout`);
  }
}
