import {CommentResponse, CommentsResponse} from "../domain/response";
import { InjectValue } from "typescript-ioc";
import { DIKeys } from "../modules";
import { BaseClient } from "../services";

export abstract class CommentsRepository {
  abstract createComments(
    videoId: string,
    text: string
  ): Promise<CommentResponse>;

  abstract updateComments(
    videoId: string,
    text: string
  ): Promise<CommentResponse>;

  abstract deleteComments(videoId: string): Promise<CommentResponse>;

  abstract getComments(videoId: string): Promise<CommentsResponse>;

  abstract getCommentByUserId(): Promise<CommentsResponse>;
}

export class HttpCommentsRepository implements CommentsRepository {
  private apiPath = "/api/v1/comments";
  @InjectValue(DIKeys.authClient)
  private httpClient!: BaseClient;

  createComments(videoId: string, text: string): Promise<CommentResponse> {
    return this.httpClient.post(`${this.apiPath}`, {
      videoId,
      text
    });
  }

  deleteComments(videoId: string): Promise<CommentResponse> {
    return this.httpClient.delete(
      `${this.apiPath}/${videoId}`
    );
  }

  getComments(videoId: string): Promise<CommentsResponse> {
    return this.httpClient.get(
      `${this.apiPath}/${videoId}/videos`
    );
  }

  updateComments(videoId: string, text: string): Promise<CommentResponse> {
    return this.httpClient.post(`${this.apiPath}/${videoId}`, {
      text
    });
  }
  getCommentByUserId(): Promise<CommentsResponse> {
    return this.httpClient.get(
      `${this.apiPath}/user_comments`
    );
  }
}
