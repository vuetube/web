import {HistoriesResponse, HistoryResponse} from "../domain/response";
import { InjectValue } from "typescript-ioc";
import { DIKeys } from "../modules";
import { BaseClient } from "../services";

export abstract class HistoriesRepository {
  abstract getHistories(): Promise<HistoriesResponse>;

  abstract createHistory(
    type: string,
    searchText: string,
    videoId: string
  ): Promise<HistoryResponse>;

  abstract deleteHistory(id: string): Promise<HistoryResponse>;

  abstract deleteHistories(type: string): Promise<boolean>;
}

export class HttpHistoriesRepository implements HistoriesRepository {
  private apiPath = "/api/v1/histories";
  @InjectValue(DIKeys.authClient)
  private httpClient!: BaseClient;

  createHistory(
    type: string,
    searchText: string,
    videoId: string
  ): Promise<HistoryResponse> {
    return this.httpClient.post(`${this.apiPath}`, {
      type,
      searchText,
      videoId
    });
  }

  deleteHistories(type: string): Promise<boolean> {
    return this.httpClient.delete(
      `${this.apiPath}/${type}/all`
    );
  }

  deleteHistory(id: string): Promise<HistoryResponse> {
    return this.httpClient.delete(`${this.apiPath}/${id}`);
  }

  getHistories(): Promise<HistoriesResponse> {
    return this.httpClient.get(`${this.apiPath}`);
  }
}
