import { InjectValue } from "typescript-ioc";
import { DIKeys } from "../modules";
import { BaseClient } from "../services";
import {SearchesResponse, SearchResponse} from "@core/domain/response/search.response";

export abstract class SearchRepository {
  abstract search(text: string): Promise<SearchResponse>;
}

export class HttpSearchRepository implements SearchRepository {
  private apiPath = "/api/v1/search";

  @InjectValue(DIKeys.noAuthClient)
  private httpClient!: BaseClient;

  search(text: string): Promise<SearchResponse> {
    return this.httpClient.post(`${this.apiPath}?limit=0`, {
      text
    });
  }
}
