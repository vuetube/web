import {InjectValue} from "typescript-ioc";
import {DIKeys} from "../modules";
import {BaseClient} from "../services";
import {SubscriptionResponse, SubscriptionsResponse} from "@core/domain/response/supscription.response";

export abstract class SubscriptionsRepository {
    abstract channels(subscriberId: string): Promise<SubscriptionsResponse>;

    abstract subscribers(): Promise<SubscriptionsResponse>

    abstract subscribe(channelId: string): Promise<SubscriptionResponse>;

    abstract checkSubscription(channelId: string): Promise<SubscriptionResponse>;
}

export class HttpSubscriptionsRepository implements SubscriptionsRepository {
    private apiPath = "/api/v1/subscriptions";
    @InjectValue(DIKeys.authClient)
    private httpClient!: BaseClient;


    channels(subscriberId: string): Promise<SubscriptionsResponse> {
        return this.httpClient.get(
            `${this.apiPath}/channels/${subscriberId}`
        );
    }

    subscribers(): Promise<SubscriptionsResponse> {
        const limit = 0;
        return this.httpClient.get(
            `${this.apiPath}/subscribers/?limit=${limit}`
        );
    }

    checkSubscription(channelId: string): Promise<SubscriptionResponse> {
        return this.httpClient.post(`${this.apiPath}/check`, {channelId});
    }

    subscribe(channelId: string): Promise<SubscriptionResponse> {
        return this.httpClient.post(`${this.apiPath}`, {channelId});
    }
}
