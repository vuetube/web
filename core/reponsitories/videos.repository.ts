import { VideosRequest } from "@core/domain/request";
import { BaseClient } from "@core/services";
import {VideoResponse, VideosResponse} from "@core/domain/response/videos.response";

export abstract class VideosRepository {
  abstract getAllPublicVideos(): Promise<VideosResponse>;

  abstract getAllPrivateVideos(): Promise<VideosResponse>;

  abstract uploadVideo(request: VideosRequest): Promise<VideosResponse>;

  abstract getVideo(videoId: string): Promise<VideoResponse>;

  abstract updateVideo(
    videoId: string,
    status: string,
    userId: string,
    thumbnailUrl: string,
    title: string,
    description: string,
    category: string
  ): Promise<VideosResponse>;

  abstract deleteVideo(videoId: string): Promise<VideoResponse>;

  abstract updateViews(videoId: string): Promise<VideoResponse>;

  abstract updateThumbnail(videoId: string): Promise<VideosResponse>;

  abstract latestVideo(): Promise<VideoResponse>;
}

export class HttpVideosRepository implements VideosRepository {
  private apiPath = "/api/v1/videos";

  constructor(private httpClient: BaseClient) {}

  getAllPublicVideos(): Promise<VideosResponse> {
    return this.httpClient.get(`${this.apiPath}/public`);
  }

  getAllPrivateVideos(): Promise<VideosResponse> {
    return this.httpClient.get(`${this.apiPath}/private`);
  }

  uploadVideo(request: VideosRequest): Promise<VideosResponse> {
    return this.httpClient.post(`${this.apiPath}`, request);
  }

  deleteVideo(videoId: string): Promise<VideoResponse> {
    console.log("DELETE VIDEO::", videoId);
    return this.httpClient.delete(
      `${this.apiPath}/${videoId}`
    );
  }

  getVideo(videoId: string): Promise<VideoResponse> {
    return this.httpClient.get(`${this.apiPath}/${videoId}`);
  }

  updateVideo(
    videoId: string,
    status: string,
    userId: string,
    thumbnailUrl: string,
    title: string,
    description: string,
    category: string
  ): Promise<VideosResponse> {
    return this.httpClient.put(`${this.apiPath}/${videoId}`, {
      status,
      userId,
      thumbnailUrl,
      title,
      description,
      category
    });
  }

  updateViews(videoId: string): Promise<VideoResponse> {
    return this.httpClient.put(
      `${this.apiPath}/${videoId}/views`
    );
  }

  updateThumbnail(videoId: string): Promise<VideosResponse> {
    return this.httpClient.put(
      `${this.apiPath}/${videoId}/thumbnails`
    );
  }

  latestVideo(): Promise<VideoResponse> {
    return this.httpClient.get(`${this.apiPath}/new_video`);
  }
}
