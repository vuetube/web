import { AxiosInstance, AxiosResponse } from "axios";
import { ErrorResponse, ErrorHandler } from "@core/domain/exception/index";
import { JsonUtils } from "@core/utils/json.utils";

abstract class BaseClient {
  abstract get<T>(path: string, params?: any, headers?: any): Promise<T>;

  abstract post<T>(
    path: string,
    body?: any,
    params?: any,
    headers?: any
  ): Promise<T>;

  abstract put<T>(
    path: string,
    body?: any,
    params?: any,
    headers?: any
  ): Promise<T>;

  abstract delete<T>(path: string, params?: any, headers?: any): Promise<T>;
}

class HttpClient extends BaseClient {
  private readonly client: AxiosInstance;

  constructor(client: AxiosInstance) {
    super();
    this.client = client;
  }

  delete<T>(path: string, params?: any, headers?: any): Promise<T> {
    return this.client
      .delete<string>(path, {
        params: params,
        headers: headers
      })
      .then(HttpClient.getData)
      .then<T>(HttpClient.parseJson)
      .catch(ex => HttpClient.handleError(path, ex));
  }

  get<T>(path: string, params?: any, headers?: any): Promise<T> {
    return this.client
      .get<string>(path, {
        params: params,
        headers: headers
      })
      .then(HttpClient.getData)
      .then<T>(HttpClient.parseJson)
      .catch(ex => HttpClient.handleError(path, ex));
  }

  post<T>(path: string, body?: any, params?: any, headers?: any): Promise<T> {
    return this.client
      .post<string>(path, body, {
        params: params,
        headers: headers
      })
      .then(HttpClient.getData)
      .then<T>(HttpClient.parseJson)
      .catch(ex => HttpClient.handleError(path, ex));
  }

  put<T>(path: string, body?: any, params?: any, headers?: any): Promise<T> {
    return this.client
      .put<string>(path, body, {
        params: params,
        headers: headers
      })
      .then(HttpClient.getData)
      .then<T>(HttpClient.parseJson)
      .catch(ex => HttpClient.handleError(path, ex));
  }

  private static parseJson<T>(data: string): T {
    if (data) return JsonUtils.fromJson<T>(data);
    else return {} as any;
  }

  private static getData(response: AxiosResponse<string>): string {
    return response.data;
  }

  private static handleError(path: string, reason: any): any {
    if (reason.toJSON) {
      console.log("request error", "path::", path, reason.toJSON());
    }
    if (reason instanceof ErrorResponse) {
      return Promise.reject(reason);
    }
    if (reason.response?.data) {
      const apiException = JsonUtils.fromJson<any>(reason.response.data);
      return Promise.reject(ErrorHandler.fromObject(apiException));
    } else {
      const baseException = new ErrorResponse(reason.message, 500, reason);
      return Promise.reject(baseException);
    }
  }
}

export { HttpClient, BaseClient };
