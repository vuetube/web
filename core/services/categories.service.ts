import { Inject } from "typescript-ioc";
import { CategoriesRepository } from "../reponsitories/categories.repository";
import {CategoriesResponse} from "@core/domain/response/categories.response";

export abstract class CategoriesService {
  abstract getAllCategories(): Promise<CategoriesResponse>;
}

export class CategoriesServiceImpl implements CategoriesService {
  constructor(@Inject private repository: CategoriesRepository) {}

  getAllCategories(): Promise<CategoriesResponse> {
    return this.repository.getAllCategories();
  }
}
