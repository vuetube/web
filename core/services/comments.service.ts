import {CommentResponse, CommentsResponse} from "@core/domain/response";
import { Inject } from "typescript-ioc";
import { CommentsRepository } from "@core/reponsitories/comments.repository";

export abstract class CommentsService {
  abstract createComments(
    videoId: string,
    text: string
  ): Promise<CommentResponse>;

  abstract updateComments(
    videoId: string,
    text: string
  ): Promise<CommentResponse>;

  abstract deleteComments(videoId: string): Promise<CommentResponse>;

  abstract getComments(videoId: string): Promise<CommentsResponse>;

  abstract getCommentByUserId(): Promise<CommentsResponse>;
}

export class CommentsServiceImpl implements CommentsService {
  constructor(@Inject private repository: CommentsRepository) {}

  createComments(videoId: string, text: string): Promise<CommentResponse> {
    return this.repository.createComments(videoId, text);
  }

  deleteComments(videoId: string): Promise<CommentResponse> {
    return this.repository.deleteComments(videoId);
  }

  getComments(videoId: string): Promise<CommentsResponse> {
    return this.repository.getComments(videoId);
  }

  updateComments(videoId: string, text: string): Promise<CommentResponse> {
    return this.repository.updateComments(videoId, text);
  }

  getCommentByUserId(): Promise<CommentsResponse> {
    return this.repository.getCommentByUserId();
  }
}
