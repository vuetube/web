import { FeelingResponse } from "@core/domain/response";
import { Inject } from "typescript-ioc";
import { FeelingsRepository } from "@core/reponsitories/feelings.repository";

export abstract class FeelingsService {
  abstract changeFeeling(
    videoId: string,
    type: string
  ): Promise<FeelingResponse>;

  abstract checkFeeling(videoId: string): Promise<FeelingResponse>;

  abstract getLikedVideo(): Promise<FeelingResponse>;
}

export class FeelingsServiceImpl implements FeelingsService {
  constructor(@Inject private repository: FeelingsRepository) {}

  changeFeeling(videoId: string, type: string): Promise<FeelingResponse> {
    return this.repository.changeFeeling(videoId, type);
  }

  checkFeeling(videoId: string): Promise<FeelingResponse> {
    return this.repository.checkFeeling(videoId);
  }

  getLikedVideo(): Promise<FeelingResponse> {
    return this.repository.getLikedVideo();
  }
}
