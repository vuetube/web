import {HistoriesResponse, HistoryResponse} from "../domain/response";
import { Inject } from "typescript-ioc";
import { HistoriesRepository } from "../reponsitories/histories.repository";

export abstract class HistoriesService {
  abstract getHistories(): Promise<HistoriesResponse>;

  abstract createHistory(
    type: string,
    searchText: string,
    videoId: string
  ): Promise<HistoryResponse>;

  abstract deleteHistory(id: string): Promise<HistoryResponse>;

  abstract deleteHistories(type: string): Promise<boolean>;
}

export class HistoriesServiceImpl implements HistoriesService {
  constructor(@Inject private repository: HistoriesRepository) {}

  createHistory(
    type: string,
    searchText: string,
    videoId: string
  ): Promise<HistoryResponse> {
    return this.repository.createHistory(type, searchText, videoId);
  }

  deleteHistories(type: string): Promise<boolean> {
    return this.repository.deleteHistories(type);
  }

  deleteHistory(id: string): Promise<HistoryResponse> {
    return this.repository.deleteHistory(id);
  }

  getHistories(): Promise<HistoriesResponse> {
    return this.repository.getHistories();
  }
}
