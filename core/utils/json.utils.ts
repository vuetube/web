import { camelCase, isArray, isObject, snakeCase } from "lodash";

export abstract class JsonUtils {
  private static serializeKeyTransform = snakeCase;
  private static deserializeKeyTransform = camelCase;

  static SerializeKeysTo(transform: (key: string) => string): void {
    this.serializeKeyTransform = transform;
  }

  static DeserializeKeysFrom(transform: (key: string) => string): void {
    this.deserializeKeyTransform = transform;
  }

  private static convertKeys<T>(
    data: any,
    transform: (key: string) => string
  ): T {
    if (isArray(data)) {
      return data.map(innerObj =>
        this.convertKeys<any>(innerObj, transform)
      ) as any;
    } else if (isObject(data)) {
      const newObj: { [key: string]: any } = {};
      Object.entries(data).forEach(([key, value]) => {
        const newKey = transform(key);
        newObj[newKey] = this.convertKeys(value, transform);
      });
      return newObj as any;
    } else {
      return data;
    }
  }

  static toJson(value: any): string {
    return JSON.stringify(this.serializerKeys(value));
  }
  static toCamelKey(value: any): string {
    return JSON.stringify(this.deserializeKeys(value));
  }
  static serializerKeys(value?: any): any {
    if (value) {
      return this.convertKeys<any>(value, this.serializeKeyTransform);
    }
  }
  static deserializeKeys(value?: any): any {
    if (value) {
      return this.convertKeys<any>(value, this.deserializeKeyTransform);
    }
  }
  static fromJson<T>(data: string | object): T {
    if (typeof data === "string") {
      const json = JSON.parse(data);
      return this.toObject(json);
    } else {
      return this.toObject(data);
    }
  }

  private static toObject<T>(data: any): T {
    if (data) {
      return this.convertKeys<T>(data, this.deserializeKeyTransform);
    } else {
      throw new Error("Can't from json");
    }
  }

  static mergeDeep = (target: any, source: any) => {
    const isObject = (obj: any) => obj && typeof obj === "object";

    if (!isObject(target) || !isObject(source)) {
      return source;
    }

    Object.keys(source).forEach(key => {
      const targetValue = target[key];
      const sourceValue = source[key];

      if (Array.isArray(targetValue) && Array.isArray(sourceValue)) {
        target[key] = targetValue.concat(sourceValue);
      } else if (isObject(targetValue) && isObject(sourceValue)) {
        target[key] = JsonUtils.mergeDeep(
          Object.assign({}, targetValue),
          sourceValue
        );
      } else {
        target[key] = sourceValue;
      }
    });

    return target;
  };
}
