import { DI, DIKeys } from "@core/modules";

export abstract class UrlUtils {
  static getFullUrl(path: string): string {
    const staticHost = DI.get<string>(DIKeys.staticHost);
    return staticHost + path;
  }

  static createLinkShare(
    type: "dashboard" | "directory",
    id: number,
    token: string
  ) {
    if (process.env.NODE_ENV === "production") {
      const host = DI.get<string>(DIKeys.apiHost);
      return `${host}/${type}/${id}?token=${token}`;
    } else {
      return `http://localhost:8080/${type}/${id}?token=${token}`;
    }
  }
}
