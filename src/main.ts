import Vue from "vue";
import App from "./App.vue";
import router from "@/router";
import store from "@/store";
import vuetify from "./plugins/vuetify";
import firebase from "./plugins/firebase";
import Vuelidate from "vuelidate";
import { Component } from "vue-property-decorator";
import "@babel/polyfill";
import "roboto-fontface/css/roboto/roboto-fontface.css";
import "@mdi/font/css/materialdesignicons.css";
import { devModule, DI, prodModule } from "@core/modules";
import { AuthenticationModule } from "@/store/modules/authentication.store";
import { ComponentUtils } from "@/shared/utils/component.utils";

switch (process.env.NODE_ENV) {
  case "production":
    DI.init([prodModule]);
    console.log("production");
    break;
  default:
    DI.init([devModule]);
    console.log("dev");
}

Vue.use(Vuelidate);

Vue.config.productionTip = false;
Vue.config.performance = true;

const requireComponents = require.context("@/shared/components", false);
ComponentUtils.registerComponentsAsGlobal(requireComponents);
// TODO: register global hook
Component.registerHooks(["beforeRouteEnter", "beforeRouteLeave"]);
AuthenticationModule.router = router;

new Vue({
  store,
  router,
  vuetify,
  firebase,
  render: h => h(App)
}).$mount("#app");
