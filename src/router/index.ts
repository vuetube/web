import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import NavBar from "@/shared/components/NavBar/NavBar.vue";
import StudioNavBar from "@/shared/components/NavBar/StudioNavBar.vue";
import { RouteUtils } from "@/shared/utils/routes.utils";
import { Routes } from "@/shared/enums";
import { DataManager } from "@core/services";
import { DI } from "@core/modules";

Vue.use(VueRouter);

const Home = () => import(/* webpackChunkName: "Home" */ "@/screens/Home.vue");
const History = () =>
  import(/* webpackChunkName: "Home" */ "@/screens/History.vue");
const Search = () =>
  import(/* webpackChunkName: "Home" */ "@/screens/Search.vue");
const Trending = () =>
  import(/* webpackChunkName: "Home" */ "@/screens/Trending.vue");
const Subscription = () =>
  import(/* webpackChunkName: "Home" */ "@/screens/Subscription.vue");
const Watch = () =>
  import(/* webpackChunkName: "Home" */ "@/screens/Watch/Watch.vue");

const ChannelHome = () =>
  import(
    /* webpackChunkName: "Channel" */ "@/screens/Channel/views/ChannelHome.vue"
  );
const Channels = () =>
  import(/* webpackChunkName: "Channel" */ "@/screens/Channel/views/Index.vue");
const ChannelSubscriber = () =>
  import(
    /* webpackChunkName: "Channel" */ "@/screens/Channel/views/ChannelSubscriber.vue"
  );
//Auth
const WelcomePage = () =>
  import(
    /* webpackChunkName: "welcome" */ "@/screens/Auth/views/WelcomePage.vue"
  );
const resetPassword = () =>
  import(
    /* webpackChunkName: "welcome" */ "@/screens/Auth/views/ResetPassword.vue"
  );
//Studio
const Studio = () =>
  import(/* webpackChunkName: "Studio" */ "@/screens/Studio/views/index.vue");
const Dashboard = () =>
  import(
    /* webpackChunkName: "Studio" */ "@/screens/Studio/views/Dashboard.vue"
  );
const Analytics = () =>
  import(
    /* webpackChunkName: "Studio" */ "@/screens/Studio/views/Analytics.vue"
  );
const AudioLibrary = () =>
  import(
    /* webpackChunkName: "Studio" */ "@/screens/Studio/views/AudioLibrary.vue"
  );
const Comments = () =>
  import(
    /* webpackChunkName: "Studio" */ "@/screens/Studio/views/Comments.vue"
  );
const Details = () =>
  import(/* webpackChunkName: "Studio" */ "@/screens/Studio/views/Details.vue");
const Monetization = () =>
  import(
    /* webpackChunkName: "Studio" */ "@/screens/Studio/views/Monetization.vue"
  );
const Playlist = () =>
  import(
    /* webpackChunkName: "Studio" */ "@/screens/Studio/views/Playlist.vue"
  );
const Subtitles = () =>
  import(
    /* webpackChunkName: "Studio" */ "@/screens/Studio/views/Subtitles.vue"
  );
const Video = () =>
  import(/* webpackChunkName: "Studio" */ "@/screens/Studio/views/Video.vue");

//NotFound
const NotFound = () =>
  import(/* webpackChunkName: "not-found" */ "@/screens/NotFound.vue");

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: Routes.home,
    components: {
      NavBar,
      default: Home
    }
  },
  {
    path: "/history",
    name: Routes.history,
    components: {
      NavBar,
      default: History
    }
  },
  {
    path: "/search",
    name: Routes.search,
    components: {
      NavBar,
      default: Search
    }
  },
  {
    path: "/trending",
    name: Routes.trending,
    components: {
      NavBar,
      default: Trending
    }
  },
  {
    path: "/subscription",
    name: Routes.subscription,
    components: {
      NavBar,
      default: Subscription
    }
  },
  {
    path: "/watch/:id",
    name: Routes.watch,
    components: {
      NavBar,
      default: Watch
    }
  },

  //Channel
  {
    path: "/channels",
    name: Routes.channels,
    components: {
      NavBar,
      default: Channels
    },
    children: [
      {
        path: "myChannel",
        name: Routes.channelHome,
        component: ChannelHome
      },
      {
        path: "subscriber/:id",
        name: Routes.channelSubscriber,
        component: ChannelSubscriber
      }
    ]
  },

  //Auth
  {
    path: "/login",
    name: Routes.welcomePage,
    component: WelcomePage
  },
  {
    path: "/reset_password/:token",
    name: Routes.resetPassword,
    component: resetPassword
  },

  //Studio
  {
    path: "/studio",
    components: {
      StudioNavBar,
      default: Studio
    },
    children: [
      {
        path: "/",
        name: Routes.dashboard,
        component: Dashboard
      },
      {
        path: "videos",
        name: Routes.videos,
        component: Video
      },
      {
        path: "details/:id",
        name: Routes.details,
        component: Details
      },
      {
        path: "analytics",
        name: Routes.analytics,
        component: Analytics
      },
      {
        path: "audio-library",
        name: Routes.audioLibrary,
        component: AudioLibrary
      },
      {
        path: "comments",
        name: Routes.comments,
        component: Comments
      },
      {
        path: "monetization",
        name: Routes.monetization,
        component: Monetization
      },
      {
        path: "playlists",
        name: Routes.playlist,
        component: Playlist
      },
      {
        path: "subtitles",
        name: Routes.subtitles,
        component: Subtitles
      }
    ]
  },

  //Not Found
  {
    path: "/notfound",
    name: Routes.notfound,
    component: NotFound,
    props: true
  },
  {
    path: "*",
    redirect: { name: Routes.notfound, params: { resource: "page" } }
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});
router.beforeEach(async (to, from, next) => {
  next();
  console.log(
    `Router::beforeEach:: ${to.name} from ${from.name}\n  at ${new Date(
      Date.now()
    ).toDateString()}`
  );
  if (RouteUtils.isRoot(to.name) || RouteUtils.isNotNeedSession(to.name!)) {
    // TODO: check again
    // if have Session go to HomePage
    const session = DI.get(DataManager).getSession();
    if (session) {
      next();
      console.log("Co session", to.name);
    }
  }
});
export default router;
