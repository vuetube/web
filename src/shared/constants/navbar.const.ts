import { DataManager } from "@core/services";
import { DI } from "@core/modules";

export class NavbarConst {
  static ITEMS = [
    {
      header: null,
      pages: [
        { title: "Home", link: "/", icon: "mdi-home" },
        { title: "Trending", link: "/trending", icon: "mdi-fire" },
        {
          title: "Subscriptions",
          link: "/subscription",
          icon: "mdi-youtube-subscription"
        }
      ]
    },
    {
      header: null,
      pages: [
        {
          title: "Library",
          link: "#l",
          icon: "mdi-play-box-multiple"
        },
        {
          title: "History",
          link: "/history",
          icon: "mdi-history"
        },
        {
          title: "Your videos",
          link: `/channels/myChannel`,
          icon: "mdi-play-box-outline"
        },

        {
          title: "Watch later",
          link: "#wl",
          icon: "mdi-clock"
        },

        {
          title: "Liked videos",
          link: "#lw",
          icon: "mdi-thumb-up"
        }
      ]
    },
    {
      header: "Subscriptions",
      pages: [{}]
    },
    {
      header: "MORE FROM DragoZero",
      pages: [
        {
          title: "DragoZero Premium",
          link: "#vp",
          icon: "mdi-youtube"
        },
        {
          title: "Gaming",
          link: "#g",
          icon: "mdi-youtube-gaming"
        },
        {
          title: "Live",
          link: "#li",
          icon: "mdi-access-point"
        }
      ]
    },
    {
      header: null,
      pages: [
        {
          title: "Setting",
          link: "#sg",
          icon: "mdi-cog"
        },
        {
          title: "Report history",
          link: "#rh",
          icon: "mdi-flag"
        },
        {
          title: "Help",
          link: "#hp",
          icon: "mdi-help-circle"
        },
        {
          title: "Send feedback",
          link: "#f",
          icon: "mdi-message-alert"
        }
      ]
    }
  ];
  static readonly ITEMS_NO_AUTH = [
    {
      header: null,
      pages: [
        { title: "Home", link: "/", icon: "mdi-home" },
        { title: "Trending", link: "/trending", icon: "mdi-fire" },
        {
          title: "Subscriptions",
          link: "/login",
          icon: "mdi-youtube-subscription"
        }
      ]
    }
  ];
  static readonly ITEMS_MINI_VARIANT = [
    {
      pages: [
        { title: "Home", link: "/", icon: "mdi-home" },
        { title: "Trending", link: "/trending", icon: "mdi-fire" },
        {
          title: "Subscriptions",
          link: "#s",
          icon: "mdi-youtube-subscription"
        },
        {
          title: "Library",
          link: "#l",
          icon: "mdi-play-box-multiple"
        }
      ]
    }
  ];
  static readonly LINKS = [
    { text: "About", link: "#" },
    { text: "Press", link: "#" },
    { text: "Copyright", link: "#" },
    { text: "Contact us", link: "#" },
    { text: "Creators", link: "#" },
    { text: "Advertise", link: "#" },
    { text: "Developers", link: "#" },
    { text: "Terms", link: "#" },
    { text: "Privacy", link: "#" },
    { text: "Policy & Safety", link: "#" },
    { text: "Test new features", link: "#" }
  ];
  static readonly SIDEBAR_ITEMS = [
    {
      header: "scroll",
      pages: [
        {
          title: "Dashboard",
          link: "/studio",
          icon: "mdi-view-dashboard"
        },
        {
          title: "Videos",
          link: "/studio/videos",
          icon: "mdi-play-box-multiple"
        },
        {
          title: "Playlists",
          link: "/studio/playlists",
          icon: "mdi-playlist-play"
        },
        {
          title: "Analytics",
          link: "/studio/analytics",
          icon: "mdi-poll-box"
        },
        {
          title: "Comments",
          link: "/studio/comments",
          icon: "mdi-message-reply-text"
        },

        {
          title: "Subtitles",
          link: "/studio/subtitles",
          icon: "mdi-subtitles"
        },
        {
          title: "Monetization",
          link: "/studio/monetization",
          icon: "mdi-currency-usd"
        },
        {
          title: "Audio library",
          link: "/studio/audio-library",
          icon: "mdi-music-box-multiple"
        }
      ]
    },
    {
      header: "fixed",
      pages: [
        {
          title: "Settings",
          link: "",
          icon: "mdi-cog"
        },
        {
          title: "Send feedback",
          link: "#sf",
          icon: "mdi-history"
        },
        {
          title: "Creator Studio Classic",
          link: "#cs",
          icon: "mdi-play-box-outline"
        }
      ]
    }
  ];
}
