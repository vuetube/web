export class PopoverInfoConst {
  static readonly YOUR_CHANNEL = "Your channel";
  static readonly VUE_TUBE_STUDIO = "DragoZero studio";
  static readonly SIGN_OUT = "Sign out";
}
