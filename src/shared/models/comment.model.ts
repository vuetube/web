import { UserInfo } from "@core/domain/models/user_info.model";
import { Reply } from "@core/domain/models/reply.model";
import { VideoType } from "@core/domain/models/video.model";
import { VideoModel } from "@/shared/models/video.model";

export class CommentModel {
  text!: string;
  userId!: UserInfo;
  videoId!: VideoModel;
  createdAt!: string;
  updatedAt!: string;
  replies!: Reply[];
  id!: string;
}
