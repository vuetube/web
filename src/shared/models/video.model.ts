import { UserModel } from "@/shared/models/user.model";

export class VideoModel {
  name!: string;
  visibility!: string;
  restrictions!: string;
  views!: number;
  comments!: number;
  thumbnail!: string;
  createdAt!: string;
  description!: string;
  "likes (vs. dislikes)"!: number;
  likes!: number;
  dislikes!: number;
  id!: string;
  url!: string;
  linkVideo!: string;
  userId!: UserModel;
}
