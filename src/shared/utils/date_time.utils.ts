export class FormatDateTime {
  static formatDate(dates: Date): string {
    const date = new Date(dates);
    let day = date.getDate().toString();
    let month = (date.getMonth() + 1).toString();
    const year = date.getFullYear().toString();
    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;
    const dateTime = `${day}-${month}-${year}`;
    return dateTime;
  }
  static formatDateTime(dates: Date): string {
    const date = new Date(dates);
    let day = date.getDate().toString();
    let month = (date.getMonth() + 1).toString();
    const year = date.getFullYear().toString();
    const hour = date.getHours().toString();
    const minute = date.getMinutes().toString();
    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;
    const dateTime = `${day}${month}${year}, ${hour}:${minute}`;
    return dateTime;
  }
}
