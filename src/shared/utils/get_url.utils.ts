export class GetUrl {
  static userId(url: string): string {
    const decodeUrl = decodeURIComponent(url);
    const indexVideos = decodeUrl.indexOf("videos/");
    const indexThumbnail = decodeUrl.indexOf("thumbnail/");
    //index "videos" length() = 6 + 1
    const userId = decodeUrl.slice(indexVideos + 7, indexThumbnail - 1);
    return userId;
  }

  static nameFile(url: string): string {
    const decodeUrl = decodeURIComponent(url);
    const indexThumbnail = decodeUrl.indexOf("thumbnail/");
    const indexAltQuery = decodeUrl.indexOf("?alt");
    // index "Thumbnail" length() = 9 + 1
    const nameFile = decodeUrl.slice(indexThumbnail + 10, indexAltQuery);
    return nameFile;
  }

  static nameVideo(url: string): string {
    const decodeUrl = decodeURIComponent(url);
    const indexVideo = decodeUrl.indexOf("video/");
    const indexAltQuery = decodeUrl.indexOf("?alt");
    // index "video"  length() = 5 + 1
    const nameFile = decodeUrl.slice(indexVideo + 6, indexAltQuery);
    console.log("NameFile::", nameFile);
    return nameFile;
  }
}
