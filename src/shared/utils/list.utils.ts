export abstract class ListUtils {
  static isEmpty(list: any) {
    return !Array.isArray(list) || !list.length;
  }

  static isNotEmpty(list: any) {
    return Array.isArray(list) && !!list.length;
  }

  static remove<T>(
    list: T[],
    isRemove: (item: T, index: number) => boolean
  ): T[] {
    if (this.isEmpty(list)) {
      return [];
    } else {
      return list.filter((item, index) => !isRemove(item, index));
    }
  }

  static swap<T>(list: T[], oldIndex: number, newIndex: number) {
    if (ListUtils.isNotEmpty(list)) {
      const temp = list[oldIndex];
      list[oldIndex] = list[newIndex];
      list[newIndex] = temp;
    }
    return list;
  }
}
