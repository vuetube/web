export abstract class ObjectUtils {
  static isEmpty(obj: object): boolean {
    return Object.keys(obj).length === 0;
  }
}
