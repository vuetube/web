import { Routes as Routers } from "@/shared/enums";
import { DataManager } from "@core/services";
import { DI } from "@core/modules";
import { StringUtils } from "@/shared/utils/string.utils";
import { isString } from "lodash";
import { Route } from "vue-router";

export class RouteUtils {
  static readonly routeIgnoreCheckSession = new Set<string>([
    Routers.welcomePage,
    Routers.resetPassword,
    Routers.watch
  ]);

  private static get dataManager(): DataManager {
    return DI.get(DataManager);
  }

  static readonly rootRoute = new Set<string>([Routers.baseRoute]);

  static isRoot(route?: string | null): boolean {
    return RouteUtils.rootRoute.has(route || "");
  }

  static isNotNeedSession(route: string): boolean {
    return RouteUtils.routeIgnoreCheckSession.has(route);
  }

  static checkTokenIsExist(route: Route): boolean {
    const token = this.getToken(route);
    return isString(token) && StringUtils.isNotEmpty(token);
  }

  static getToken(route: Route): string {
    return route.query.token as string;
  }

  static isLogin(): boolean {
    return !!RouteUtils.dataManager.getSession();
  }

  static isHaveToken(): boolean {
    return !!RouteUtils.dataManager.getToken();
  }
}
