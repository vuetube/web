export abstract class StringUtils {
  static isEmpty(str: string): boolean {
    return !str || !str.length;
  }

  static isNotEmpty(str: string | null): boolean {
    return !!str && !!str.length;
  }
}
