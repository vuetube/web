import { VideoType } from "@core/domain/models/video.model";
import { VideoModel } from "@/shared/models/video.model";
import { FormatDateTime } from "@/shared/utils/date_time.utils";
import { CommentsType } from "@core/domain/models/comments.model";
import { CommentModel } from "@/shared/models/comment.model";
import { Reply } from "@core/domain/models/reply.model";
import { ReplyModel } from "@/shared/models/reply.model";
import {
  SearchUserType,
  SearchVideoType
} from "@core/domain/models/search.model";
import {
  SearchUserModel,
  SearchVideoModel
} from "@/shared/models/searchUserModel";
import { History } from "@core/domain/models/history.model";
import { HistoryModel } from "@/shared/models/history.model";

export class Transform {
  static toVideoModel(video: VideoType): VideoModel {
    return {
      name: video.title,
      visibility: video.status,
      restrictions: "none",
      views: video.views,
      comments: video.comments,
      thumbnail: video.thumbnailUrl,
      createdAt: FormatDateTime.formatDate(video.createdAt),
      description: video.description,
      "likes (vs. dislikes)": video.likes + video.dislikes,
      likes: video.likes,
      dislikes: video.dislikes,
      id: video.id,
      linkVideo: video.url,
      url: `/watch/${video.id}`,
      userId: {
        name: video.userId.channelName,
        photoUrl: video.userId.photoUrl,
        url: `/channels/subscriber/${video.userId.id}`,
        subscribers: video.userId.subscribers,
        id: video.userId.id
      }
    };
  }
  static toLatestVideoModel(video: VideoType): VideoModel {
    return {
      name: video.title,
      visibility: video.status,
      restrictions: "none",
      views: video.views,
      comments: video.comments,
      thumbnail: video.thumbnailUrl,
      createdAt: FormatDateTime.formatDateTime(video.createdAt),
      description: video.description,
      "likes (vs. dislikes)": video.likes + video.dislikes,
      likes: video.likes,
      dislikes: video.dislikes,
      id: video.id,
      linkVideo: video.url,
      url: `/watch/${video.id}`,
      userId: {
        name: video.userId.channelName,
        photoUrl: video.userId.photoUrl,
        url: `/channels/subscriber/${video.userId.id}`,
        subscribers: video.userId.subscribers,
        id: video.userId.id
      }
    };
  }
  static toCommentModel(comment: CommentsType): CommentModel {
    return {
      text: comment.text,
      userId: comment.userId,
      videoId: {
        name: comment.videoId.title,
        visibility: comment.videoId.status,
        restrictions: "none",
        views: comment.videoId.views,
        comments: comment.videoId.comments,
        thumbnail: comment.videoId.thumbnailUrl,
        createdAt: FormatDateTime.formatDate(comment.videoId.createdAt),
        description: comment.videoId.description,
        "likes (vs. dislikes)":
          comment.videoId.likes + comment.videoId.dislikes,
        likes: comment.videoId.likes,
        dislikes: comment.videoId.dislikes,
        id: comment.videoId.id,
        linkVideo: comment.videoId.url,
        url: `/watch/${comment.videoId.id}`,
        userId: {
          name: comment.userId.channelName,
          photoUrl: comment.userId.photoUrl,
          url: `/channels/subscriber/${comment.userId.id}`,
          subscribers: comment.userId.subscribers,
          id: comment.userId.id
        }
      },
      createdAt: FormatDateTime.formatDate(comment.createdAt),
      updatedAt: FormatDateTime.formatDate(comment.updatedAt),
      replies: comment.replies,
      id: comment.id
    };
  }

  static toRepliesModel(reply: Reply): ReplyModel {
    return {
      id: reply.id,
      text: reply.text,
      userId: reply.userId,
      commentsId: reply.commentsId,
      createdAt: FormatDateTime.formatDate(reply.createdAt),
      updatedAt: FormatDateTime.formatDate(reply.updatedAt)
    };
  }

  static toSearchVideoModel(data: SearchVideoType): SearchVideoModel {
    return {
      createdAt: FormatDateTime.formatDate(data.createdAt),
      description: data.description,
      id: data.id,
      status: data.status,
      thumbnailUrl: data.thumbnailUrl,
      title: data.title,
      updatedAt: FormatDateTime.formatDate(data.updatedAt),
      url: data.url,
      link: `/watch/${data.id}`,
      views: data.views,
      userId: {
        name: data.userId.channelName,
        photoUrl: data.userId.photoUrl,
        url: `/channels/${data.userId.id}`,
        subscribers: data.userId.subscribers,
        id: data.userId.id
      }
    };
  }

  static toSearchUserModel(data: SearchUserType): SearchUserModel {
    return {
      link: `/channels/subscriber/${data.id}`,
      photoUrl: data.photoUrl,
      channelName: data.channelName,
      email: data.email,
      createdAt: FormatDateTime.formatDate(data.createdAt),
      updatedAt: FormatDateTime.formatDate(data.updatedAt),
      subscribers: data.subscribers,
      videos: data.videos?.map(value => {
        return Transform.toVideoModel(value);
      }),
      id: data.id
    };
  }

  static toHistoryModel(history: History): HistoryModel {
    if (history.videoId)
      return {
        id: history.id,
        type: history.type,
        searchText: history.searchText,
        videoId: {
          name: history.videoId.title,
          visibility: history.videoId.status,
          restrictions: "none",
          views: history.videoId.views,
          comments: history.videoId.comments,
          thumbnail: history.videoId.thumbnailUrl,
          createdAt: FormatDateTime.formatDate(history.videoId.createdAt),
          description: history.videoId.description,
          "likes (vs. dislikes)":
            history.videoId.likes + history.videoId.dislikes,
          likes: history.videoId.likes,
          dislikes: history.videoId.dislikes,
          id: history.videoId.id,
          linkVideo: history.videoId.url,
          url: `/watch/${history.videoId.id}`,
          userId: {
            name: history.userId.channelName,
            photoUrl: history.userId.photoUrl,
            url: `/channels/subscriber/${history.userId.id}`,
            subscribers: history.userId.subscribers,
            id: history.userId.id
          }
        },
        userId: {
          name: history.userId.channelName,
          photoUrl: history.userId.photoUrl,
          url: `/channels/${history.userId.id}`,
          subscribers: history.userId.subscribers,
          id: history.userId.id
        },
        createdAt: FormatDateTime.formatDate(history.createdAt),
        updatedAt: FormatDateTime.formatDate(history.updatedAt)
      };
    return {
      id: history.id,
      type: history.type,
      searchText: history.searchText,
      userId: {
        name: history.userId.channelName,
        photoUrl: history.userId.photoUrl,
        url: `/channels/${history.userId.id}`,
        subscribers: history.userId.subscribers,
        id: history.userId.id
      },
      createdAt: FormatDateTime.formatDate(history.createdAt),
      updatedAt: FormatDateTime.formatDate(history.updatedAt)
    };
  }
}
