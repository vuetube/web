import Vue from "vue";
import Vuex from "vuex";
import VuexPersistence from "vuex-persist";
//State
import { AuthenticationState } from "@/store/modules/authentication.store";
import { UserInfoState } from "@/store/modules/user_data.store";
import { VideoState } from "@/store/modules/video.store";
import { CategoriesState } from "@/store/modules/category.store";
import { CommentState } from "@/store/modules/comment.store";
import { FeelingState } from "@/store/modules/feeling.store";
import { SubscriptionState } from "@/store/modules/subscription.store";
import { SearchState } from "@/store/modules/search.store";
import { HistoryState } from "@/store/modules/history.store";

Vue.use(Vuex);

export interface RootState {
  authenticationState: AuthenticationState;
  userInfoState: UserInfoState;
  videoState: VideoState;
  categoriesState: CategoriesState;
  commentState: CommentState;
  feelingState: FeelingState;
  subscriptionState: SubscriptionState;
  searchState: SearchState;
  historyState: HistoryState;
}

const debug = process.env.NODE_ENV !== "production";
const vuexLocal = new VuexPersistence<RootState>({
  storage: window.localStorage
});
const store = new Vuex.Store<RootState>({
  strict: debug
});
export default store;
