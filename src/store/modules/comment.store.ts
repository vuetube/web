import {
  Action,
  getModule,
  Module,
  Mutation,
  VuexModule
} from "vuex-module-decorators";
import store from "@/store";
import { Stores } from "@/shared/enums";
import { Inject } from "typescript-ioc";
import { CommentsService } from "@core/services/comments.service";
import { VideosModule } from "@/store/modules/video.store";
import { CommentsType } from "@core/domain/models/comments.model";
import { CommentModel } from "@/shared/models/comment.model";
import { Transform } from "@/shared/utils/transform.utils";

export interface CommentState {
  commentsByVideo: CommentsType[];
  commentByUser: CommentsType[];
}

@Module({
  store: store,
  name: Stores.commentStore,
  dynamic: true,
  namespaced: true
})
export class CommentStore extends VuexModule {
  commentsByVideo: CommentState["commentsByVideo"] = [];
  commentByUser: CommentState["commentByUser"] = [];
  @Inject
  commentsService!: CommentsService;

  get getComments() {
    let comments: CommentModel[] = [];
    this.commentsByVideo.map(value => {
      if (value) {
        comments = [...comments, Transform.toCommentModel(value)];
      }
    });
    return comments;
  }

  get getCommentsByUser() {
    let comments: CommentModel[] = [];
    this.commentByUser.map(value => {
      if (value) {
        comments = [...comments, Transform.toCommentModel(value)];
      }
    });
    return comments;
  }

  @Mutation
  setComments(comments: CommentsType[]) {
    this.commentsByVideo = [...comments];
  }

  @Mutation
  setCommentsByUser(comments: CommentsType[]) {
    this.commentByUser = [...comments];
  }

  @Action
  async getCommentsByVideo(videoId: string) {
    try {
      const response = await this.commentsService.getComments(videoId);
      if (response) {
        this.setComments(response.results);
      }
    } catch (e) {
      console.log("Cannot get comments ::", e);
    }
  }

  @Action
  async createComment(payload: any) {
    try {
      const { videoId, text } = payload;
      await this.commentsService.createComments(videoId, text);
    } catch (e) {
      console.log("Cannot create comments ::", e);
    }
  }

  @Action
  async updateComment(text: string) {
    try {
      const videoId = VideosModule.getVideo.id;
      await this.commentsService.updateComments(videoId, text);
    } catch (e) {
      console.log("Cannot update comments ::", e);
    }
  }

  @Action
  async deleteComment(text: string) {
    try {
      const videoId = VideosModule.getVideo.id;
      await this.commentsService.deleteComments(videoId);
    } catch (e) {
      console.log("cannot delete comments", e);
    }
  }

  @Action
  async getCommentByUserId() {
    try {
      const response = await this.commentsService.getCommentByUserId();
      if (response) {
        this.setCommentsByUser(response.results);
      }
      console.log(response.results);
    } catch (e) {
      console.log("Cannot get comment by user id", e);
    }
  }
}

export const CommentsModule = getModule(CommentStore);
