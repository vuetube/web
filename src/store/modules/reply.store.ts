import {
  Action,
  getModule,
  Module,
  Mutation,
  VuexModule
} from "vuex-module-decorators";
import store from "@/store";
import { Stores } from "@/shared/enums";
import { Reply } from "@core/domain/models/reply.model";
import { Inject } from "typescript-ioc";
import { RepliesService } from "@core/services/replies.service";

export interface ReplyState {
  replies: Reply[];
}

@Module({
  store: store,
  name: Stores.replyStore,
  dynamic: true,
  namespaced: true
})
export class ReplyStore extends VuexModule {
  replies: ReplyState["replies"] = [];

  @Inject
  repliesService!: RepliesService;

  get getReplies() {
    return this.replies;
  }

  @Mutation
  setReplies(reply: Reply[]) {
    this.replies = [...reply];
  }

  @Action
  async getReply() {
    try {
      const responsive = await this.repliesService.getReply();
      this.setReplies(responsive.results);
    } catch (e) {
      console.log("Cannot get replies::", e);
    }
  }

  @Action
  async createReply(payload: any) {
    try {
      const { commentId, text } = payload;
      const responsive = await this.repliesService.createReply(commentId, text);
      console.log(responsive);
      console.log("Reply::");
    } catch (e) {
      console.log("Cannot get replies::", e);
    }
  }

  @Action
  async updateReply() {
    try {
      // const responsive = await this.repliesService.getReply()
      console.log("Reply::");
    } catch (e) {
      console.log("Cannot update replies::", e);
    }
  }

  @Action
  async deleteReply() {
    try {
      // const responsive = await this.repliesService.getReply()
      console.log("Reply::");
    } catch (e) {
      console.log("Cannot delete replies::", e);
    }
  }
}

export const ReplyModule = getModule(ReplyStore);
