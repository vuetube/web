import {
    Action,
    getModule,
    Module,
    Mutation,
    VuexModule
} from "vuex-module-decorators";
import store from "@/store";
import {Stores} from "@/shared/enums";
import {
    SearchUserType,
    SearchVideoType
} from "@core/domain/models/search.model";
import {Inject} from "typescript-ioc";
import {SearchService} from "@core/services/search.service";
import {Transform} from "@/shared/utils/transform.utils";
import {
    SearchUserModel,
    SearchVideoModel
} from "@/shared/models/searchUserModel";
import {SearchesResponse, SearchResponse} from "@core/domain/response/search.response";

export interface SearchState {
    searchVideo: SearchVideoType[];
    searchUser: SearchUserType[];
}

@Module({
    store: store,
    name: Stores.searchStore,
    dynamic: true,
    namespaced: true
})
export class SearchStore extends VuexModule {
    _searchUser: SearchState["searchUser"] = [];
    _searchVideo: SearchState["searchVideo"] = [];
    @Inject
    searchService!: SearchService;

    get getSearchVideo() {
        let search: SearchVideoModel[] = [];
        this._searchVideo.map(value => {
            search = [...search, Transform.toSearchVideoModel(value)];
        });
        return search;
    }

    get getSearchUser() {
        let search: SearchUserModel[] = [];
        this._searchUser.map(value => {
            search = [...search, Transform.toSearchUserModel(value)];
        });
        return search;
    }

    @Mutation
    saveSearchVideo(data: SearchVideoType[]) {
        this._searchVideo = [...data];
    }

    @Mutation
    saveSearchUser(data: SearchUserType[]) {
        this._searchUser = [...data];
    }

    @Action
    async search(query: string) {
        try {
            const response = await this.searchService.search(query);
            this.saveSearchVideo(response.results.videos);
            this.saveSearchUser(response.results.users);
        } catch (e) {
            console.log("Cannot search ::", e);
        }
    }
}

export const SearchModule = getModule(SearchStore);
