import {
    Action,
    getModule,
    Module,
    Mutation,
    VuexModule
} from "vuex-module-decorators";
import store from "@/store";
import {Stores} from "@/shared/enums";
import {Inject, InjectValue} from "typescript-ioc";
import {SubscriptionsService} from "@core/services/subscriptions.service";
import {AuthenticationService, DataManager} from "@core/services";
import {DI, DIKeys} from "@core/modules";
import {UserDataModule} from "@/store/modules/user_data.store";
import {SubscriptionType} from "@core/domain/models/subscription.model";
import {ListUtils} from "@/shared/utils/list.utils";
import {VideoModel} from "@/shared/models/video.model";
import {Transform} from "@/shared/utils/transform.utils";
import {VideoType} from "@core/domain/models/video.model";
import {StringUtils} from "@/shared/utils/string.utils";
import {UserInfo} from "@core/domain/models/user_info.model";

export interface SubscriptionState {
    status: SubscribeStatus;
    listSubscribers: SubscriptionType[];
    videosSubscriber: VideoType[];
    videos: VideoType[];
    info: UserInfo
}

export enum SubscribeStatus {
    undefined,
    subscribed,
    unsubscribed
}

@Module({
    store: store,
    name: Stores.subscriptionStore,
    dynamic: true,
    namespaced: true
})
export class SubscriptionStore extends VuexModule {
    _status: SubscriptionState["status"] = SubscribeStatus.undefined;
    _listSubscribers: SubscriptionState["listSubscribers"] = [];
    _videosSubscriber: SubscriptionState["videosSubscriber"] = [];
    _subscriberInfo: SubscriptionState["info"] = new UserInfo("",
        "",
        "",
        "",
        new Date(),
        new Date(),
        0,
        "")
    _videos: SubscriptionState["videos"] = [];
    @Inject
    subscriptionService!: SubscriptionsService;
    @InjectValue(DIKeys.noAuthClient)
    noAuthClient!: AuthenticationService;
    @Inject
    dataManager!: DataManager;

    get getVideos(): VideoModel[] {
        let videos: VideoModel[] = [];
        this._videos.map(value => {
            if (value) videos = [...videos, Transform.toVideoModel(value)];
        });
        return videos;
    }

    @Mutation
    saveVideos(video: VideoType[]) {
        this._videos = [...video];
    }

    get videosSubscriber(): VideoModel[] {
        if (ListUtils.isEmpty(this._videosSubscriber)) {
            return this.dataManager.getVideosSubscriber();
        }
        let videos: VideoModel[] = [];
        this._videosSubscriber.map(value => {
            if (value) {
                videos = [...videos, Transform.toVideoModel(value)];
            }
        });
        return videos;
    }

    @Mutation
    setVideoSubscriber(video: VideoType[]) {
        this._videosSubscriber = [...video];
    }

    @Mutation
    setSubscriberInfo(info: UserInfo) {
        this._subscriberInfo = info;
    }

    get subscriberInfo()
    {
        return this._subscriberInfo;
    }

    get listSubscribers(): SubscriptionType[] {
        if (ListUtils.isEmpty(this._listSubscribers)) {
            return this.dataManager.getSubscribers();
        }
        return this._listSubscribers;
    }

    @Mutation
    setListSubscribers(user: SubscriptionType[]) {
        this._listSubscribers = [...user];
    }

    get status() {
        return this._status;
    }

    @Mutation
    resetStatus() {
        this._status = SubscribeStatus.undefined;
    }

    @Mutation
    setStatus(status: SubscribeStatus) {
        this._status = status;
    }

    @Action
    async checkSubscription(id: string) {
        try {
            if (StringUtils.isEmpty(id)) {
                id = DI.get(DataManager).getVideo().userId.id;
            }
            const response = await this.subscriptionService.checkSubscription(id);
            if (response.message === "OK") {
                console.log(response.message);
                this.setStatus(SubscribeStatus.subscribed);
            } else {
                console.log(response.message);
                this.setStatus(SubscribeStatus.unsubscribed);
            }
        } catch (e) {
            console.log("Cannot check subscription::", e);
            this.setStatus(SubscribeStatus.unsubscribed);
        }
    }

    @Action
    async subscribe(channelId: string) {
        try {
            const response = await this.subscriptionService.subscribe(channelId);
            if (response.message === "create") {
                this.setStatus(SubscribeStatus.subscribed);
            } else {
                this.setStatus(SubscribeStatus.unsubscribed);
            }
        } catch (e) {
            console.log("Cannot change subscription::", e);
        }
    }

    @Action
    async getSubscribers() {
        try {
            const response = await this.subscriptionService.subscribers();
            this.dataManager.saveSubscribers(response.results);
            this.setListSubscribers(response.results);
        } catch (e) {
            console.log("Cannot getSubscribers subscription::", e);
        }
    }

    @Action
    async getChannels(channelId: string) {
        try {
            const response = await this.subscriptionService.channels(channelId);
            this.dataManager.saveVideosSubscriber(response.results[0].videos);
            this.setVideoSubscriber(response.results[0].videos);
            this.setSubscriberInfo(response.results[0].channelId);
        } catch (e) {
            console.log("Cannot getChannels ::", e);
        }
    }

    @Action
    async getVideosUnsubscribed(id: string) {
        try {
            const response = await this.noAuthClient.getUser(id);
            // let data: SubscriptionType | null = null
            // this.dataManager.saveVideosSubscriber(data)
            // this.setVideoSubscriber(data)
        } catch (e) {
            console.log("Cannot getVideosUnsubscribed::", e);
        }
    }
}

export const SubscriptionModule = getModule(SubscriptionStore);
